# PredictX Challenge

By Pablo Sande Pena

## Mock API

Run `json-server --watch ./src/assets/data/hollywood.json` to set up the Mock API 

## Development server

Run `ng serve --liveReload=false` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

The `--liveReload=false` option avoids app reloading everytime the Mock Api refreshes the json file.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
