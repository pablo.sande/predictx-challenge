import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDashboard } from '../models/dashboard.interface';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardConfigService {
    constructor(private _http: HttpClient) { }

    getDashboardConfig(id): Observable<IDashboard> {
        const configUrl = `${environment.apiUrl}hollywood/${id}`;
        return this._http.get<IDashboard>(configUrl);
    }

    postDashboardConfig(dashboard: IDashboard): Observable<IDashboard> {
        const configUrl = `${environment.apiUrl}hollywood`;
        return this._http.post<IDashboard>(configUrl, dashboard);
    }

    putDashboardConfig(dashboard: IDashboard): Observable<IDashboard> {
        const configUrl = `${environment.apiUrl}hollywood/${dashboard.id}`;
        return this._http.put<IDashboard>(configUrl, dashboard);
    }
}
