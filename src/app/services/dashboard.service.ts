import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { IDashboard } from '../models/dashboard.interface';

@Injectable()
export class DashboardService {
  hollywoodUrl = `${environment.apiUrl}hollywood`;

  constructor(private _http: HttpClient) { }

  getHollywood(): Observable<IDashboard[]> {
    return this._http.get<IDashboard[]>(this.hollywoodUrl);
  }
}
