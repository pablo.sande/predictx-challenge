export interface IDashboard {
  id: number;
  name: string;
  layout: string;
  style: string;
  charts: any;
}
