import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';

import { IAppState } from '../../store/state/app.state';
import { selectSelectedDashboard } from '../../store/selectors/dashboard.selector';
import { GetDashboard } from '../../store/actions/dashboard.actions';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dashboard$ = this._store.pipe(select(selectSelectedDashboard));

  constructor(
    private _store: Store<IAppState>,
    private _route: ActivatedRoute
  ) {}

  ngOnInit() {
    this._store.dispatch(new GetDashboard(this._route.snapshot.params.id));
  }
}
