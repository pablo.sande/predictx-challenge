import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardComponent as DashboardContainerComponent } from './dashboard.component';
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
import { HollywoodComponent as HollywoodContainerComponent } from '../../containers/hollywood/hollywood.component';
import { HollywoodComponent } from '../../components/hollywood/hollywood.component';
import { ChartModule } from 'angular-highcharts';
import { AppRoutingModule } from '../../app-routing.module';
import { appReducers } from '../../store/reducers/app.reducers';
import { StoreModule } from '@ngrx/store';
import {
  MatDividerModule,
  MatListModule
} from '@angular/material';
import {APP_BASE_HREF} from '@angular/common';


describe('DashboardContainerComponent', () => {
  let component: DashboardContainerComponent;
  let fixture: ComponentFixture<DashboardContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardContainerComponent,
        DashboardComponent,
        HollywoodContainerComponent,
        HollywoodComponent
      ],
      imports: [
        ChartModule,
        MatDividerModule,
        MatListModule,
        RouterTestingModule,
        AppRoutingModule,
        StoreModule.forRoot(appReducers)
      ],
      providers: [{provide: APP_BASE_HREF, useValue : '/' }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
