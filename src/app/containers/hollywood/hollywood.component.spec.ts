import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HollywoodComponent as HollywoodContainerComponent } from './hollywood.component';
import { HollywoodComponent } from '../../components/hollywood/hollywood.component';
import {
  MatDividerModule,
  MatListModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatInputModule,
  MatDialogModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import { appReducers } from '../../store/reducers/app.reducers';
import { StoreModule } from '@ngrx/store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddDashboardComponent } from '../../components/add-dashboard/add-dashboard.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { FormsModule } from '@angular/forms';

describe('HollywoodContainerComponent', () => {
  let component: HollywoodContainerComponent;
  let fixture: ComponentFixture<HollywoodContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HollywoodContainerComponent,
        HollywoodComponent,
        AddDashboardComponent
      ],
      imports: [
        MatDividerModule,
        MatListModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatDialogModule,
        MatInputModule,
        MatSelectModule,
        MatToolbarModule,
        StoreModule.forRoot(appReducers),
        RouterTestingModule,
        BrowserAnimationsModule,
        FormsModule
      ]
    }).overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [ AddDashboardComponent ],
      }
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HollywoodContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a newDashboard properly', () => {
    component.newDashboard = {
      id: null,
      name: '',
      layout: '',
      style: '',
      charts: []
    };
    component.hollywood = [];
    component.hollywood.push(component.newDashboard);
    component.openDialog();
    component.dialog.closeAll();

    expect(component.newDashboard.id).toBeGreaterThan(0);
    expect(component.newDashboard.name).toBeTruthy();
    expect(component.newDashboard.layout).toBeTruthy();
    expect(component.newDashboard.style).toBeTruthy();
  });
});
