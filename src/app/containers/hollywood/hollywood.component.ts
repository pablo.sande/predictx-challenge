import { GetHollywood, AddDashboard } from './../../store/actions/dashboard.actions';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material';

import { IAppState } from '../../store/state/app.state';
import { selectDashboardList } from '../../store/selectors/dashboard.selector';
import { Router } from '@angular/router';
import { IDashboard } from '../../models/dashboard.interface';
import { AddDashboardComponent } from '../../components/add-dashboard/add-dashboard.component';

import { Observable } from 'rxjs';

@Component({
  templateUrl: './hollywood.component.html',
  styleUrls: ['./hollywood.component.scss']
})

export class HollywoodComponent implements OnInit {
  hollywood$: Observable<IDashboard[]>;
  hollywood: IDashboard[];
  newDashboard: IDashboard;

  constructor
    (private _store: Store<IAppState>,
    private _router: Router,
    public dialog: MatDialog) {
    this.hollywood$ = this._store.select(selectDashboardList);
  }

  ngOnInit() {
    this._store.dispatch(new GetHollywood());
    this.hollywood$.subscribe(hollywood => this.hollywood = hollywood);
  }

  navigateToDashboard(id: string) {
    this._router.navigate(['dashboard', id]);
  }

  private addDashboard(result) {
    if (result && result.name && result.layout && result.style) {
      this.newDashboard.name = result.name;
      this.newDashboard.layout = result.layout;
      this.newDashboard.style = result.style;
      this._store.dispatch(new AddDashboard(this.newDashboard));
    }
  }

  openDialog(): void {
    this.newDashboard = {
      id: this.hollywood.length + 1,
      layout: 'grid2',
      style: 'dark',
      name: 'New Dashboard',
      charts: {}
    };

    const dialogRef = this.dialog.open(AddDashboardComponent, {
      width: '320px',
      data: {
        name: this.newDashboard.name,
        layout: this.newDashboard.layout,
        layouts: [ {value: 'grid1', viewValue: 'Grid 1'}, {value: 'grid2', viewValue: 'Grid 2'} ],
        style: this.newDashboard.style,
        styles: [ {value: 'dark', viewValue: 'Dark'}, {value: 'light', viewValue: 'Light'} ]
      }
    });

    dialogRef.afterClosed().subscribe(result => this.addDashboard(result));
  }
}
