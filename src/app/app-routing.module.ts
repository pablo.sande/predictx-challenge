import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HollywoodComponent } from './containers/hollywood/hollywood.component';
import { DashboardComponent } from './containers/dashboard/dashboard.component';

const routes: Routes = [
  { path: 'hollywood', component: HollywoodComponent },
  { path: 'dashboard/:id', component: DashboardComponent },
  { path: '', redirectTo: '/hollywood', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
