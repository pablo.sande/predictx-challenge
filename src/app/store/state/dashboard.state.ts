import { IDashboard } from '../../models/dashboard.interface';

export interface IDashboardState {
  hollywood: IDashboard[];
  selectedDashboard: IDashboard;
}

export const initialDashboardState: IDashboardState = {
  hollywood: null,
  selectedDashboard: null
};
