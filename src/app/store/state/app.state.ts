import { RouterReducerState } from '@ngrx/router-store';

import { IDashboardState, initialDashboardState } from './dashboard.state';
import { initialConfigState, IConfigState } from './config.state';


export interface IAppState {
  router?: RouterReducerState;
  hollywood: IDashboardState;
  config: IConfigState;
}

export const initialAppState: IAppState = {
  hollywood: initialDashboardState,
  config: initialConfigState
};

export function getInitialState(): IAppState {
  return initialAppState;
}
