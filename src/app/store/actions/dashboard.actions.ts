import { Action } from '@ngrx/store';

import { IDashboard } from '../../models/dashboard.interface';

export enum EDashboardActions {
  GetHollywood = '[Dashboard] Get Hollywood',
  GetHollywoodSuccess = '[Dashboard] Get Hollywood Success',
  GetDashboard = '[Dashboard] Get Dashboard',
  GetDashboardSuccess = '[Dashboard] Get Dashboard Success',
  AddDashboard = '[Dashboard] Add Dashboard',
  EditDashboard = '[Dashboard] Edit Dashboard'
}

export class GetHollywood implements Action {
  public readonly type = EDashboardActions.GetHollywood;
}

export class GetHollywoodSuccess implements Action {
  public readonly type = EDashboardActions.GetHollywoodSuccess;
  constructor(public payload: IDashboard[]) {}
}

export class GetDashboard implements Action {
  public readonly type = EDashboardActions.GetDashboard;
  constructor(public payload: number) {}
}

export class GetDashboardSuccess implements Action {
  public readonly type = EDashboardActions.GetDashboardSuccess;
  constructor(public payload: IDashboard) {}
}

export class AddDashboard implements Action {
  public readonly type = EDashboardActions.AddDashboard;
  constructor(public payload: IDashboard) {}
}

export class EditDashboard implements Action {
  public readonly type = EDashboardActions.EditDashboard;
  constructor(public payload: IDashboard) {}
}

export type DashboardActions = GetHollywood | GetHollywoodSuccess | GetDashboard | GetDashboardSuccess | AddDashboard | EditDashboard;
