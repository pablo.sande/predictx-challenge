import { EDashboardActions } from './../actions/dashboard.actions';
import { DashboardActions } from '../actions/dashboard.actions';
import { initialDashboardState, IDashboardState } from '../state/dashboard.state';

export const dashboardReducers = (
  state = initialDashboardState,
  action: DashboardActions
): IDashboardState => {
  switch (action.type) {
    case EDashboardActions.GetHollywoodSuccess: {
      return {
        ...state,
        hollywood: action.payload
      };
    }
    case EDashboardActions.GetDashboardSuccess: {
      return {
        ...state,
        selectedDashboard: action.payload
      };
    }

    default:
      return state;
  }
};
