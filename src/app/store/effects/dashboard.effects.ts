import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { of } from 'rxjs';
import { switchMap, map, withLatestFrom } from 'rxjs/operators';

import { IAppState } from '../state/app.state';
import {
  GetHollywoodSuccess,
  EDashboardActions,
  GetDashboardSuccess,
  GetDashboard,
  GetHollywood,
  AddDashboard,
  EditDashboard
} from '../actions/dashboard.actions';
import { DashboardService } from '../../services/dashboard.service';
import { DashboardConfigService } from '../../services/dashboard-config.service';
import { IDashboard } from '../../models/dashboard.interface';
import { selectDashboardList } from '../selectors/dashboard.selector';

@Injectable()
export class DashboardEffects {
  @Effect()
  getDashboard$ = this._actions$.pipe(
    ofType<GetDashboard>(EDashboardActions.GetDashboard),
    map(action => action.payload),
    switchMap((id) => this._dashboardConfigService.getDashboardConfig(id)),
    switchMap((dashboard: IDashboard) => of(new GetDashboardSuccess(dashboard)))
  );

  @Effect()
  addDashboard$ = this._actions$.pipe(
    ofType<AddDashboard>(EDashboardActions.AddDashboard),
    map(action => action.payload),
    switchMap((dashboard) => this._dashboardConfigService.postDashboardConfig(dashboard)),
    withLatestFrom(this._store.pipe(select(selectDashboardList))),
    switchMap(([newDashboard, hollywood]) => {
      hollywood.push(newDashboard);
      return of(new GetHollywoodSuccess(hollywood));
    })
  );

  @Effect()
  editDashboard$ = this._actions$.pipe(
    ofType<EditDashboard>(EDashboardActions.EditDashboard),
    map(action => action.payload),
    switchMap((dashboard) => this._dashboardConfigService.putDashboardConfig(dashboard)),
    switchMap((dashboard: IDashboard) => of(new GetDashboardSuccess(dashboard)))
  );

  @Effect()
  getHollywood$ = this._actions$.pipe(
    ofType<GetHollywood>(EDashboardActions.GetHollywood),
    switchMap(() => this._dashboardService.getHollywood()),
    switchMap((dashboardHttp: IDashboard[]) => of(new GetHollywoodSuccess(dashboardHttp)))
  );

  constructor(
    private _dashboardService: DashboardService,
    private _dashboardConfigService: DashboardConfigService,
    private _actions$: Actions,
    private _store: Store<IAppState>
  ) {}
}
