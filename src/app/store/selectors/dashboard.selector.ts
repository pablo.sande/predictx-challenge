import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { IDashboardState } from '../state/dashboard.state';

const selectHollywood = (state: IAppState) => state.hollywood;

export const selectDashboardList = createSelector(
  selectHollywood,
  (state: IDashboardState) => state.hollywood
);

export const selectSelectedDashboard = createSelector(
  selectHollywood,
  (state: IDashboardState) => state.selectedDashboard
);
