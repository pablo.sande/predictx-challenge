import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { BrowserModule } from '@angular/platform-browser';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatDividerModule,
  MatListModule,
  MatGridListModule,
  MatDialogModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatInputModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { appReducers } from './store/reducers/app.reducers';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { ConfigEffects } from './store/effects/config.effects';
import { DashboardEffects } from './store/effects/dashboard.effects';
import { DashboardService } from './services/dashboard.service';
import { HollywoodComponent as HollywoodContainerComponent } from './containers/hollywood/hollywood.component';
import { HollywoodComponent } from './components/hollywood/hollywood.component';
import { DashboardComponent as DashboardContainerComponent } from './containers/dashboard/dashboard.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AddDashboardComponent } from './components/add-dashboard/add-dashboard.component';
import { ChartModule } from 'angular-highcharts';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      // entryComponents: [DialogOverviewExampleDialog],
      declarations: [
        AppComponent,
        HollywoodContainerComponent,
        HollywoodComponent,
        DashboardContainerComponent,
        DashboardComponent,
        AddDashboardComponent
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot([DashboardEffects, ConfigEffects]),
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        AppRoutingModule,
        RouterTestingModule,
        ChartModule,
        FormsModule,
        ReactiveFormsModule,
        MatDividerModule,
        MatListModule,
        MatGridListModule,
        MatDialogModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatInputModule,
        MatSelectModule,
        MatToolbarModule
      ],
      providers: [DashboardService]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('PredictX Challenge');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('a').textContent).toContain('PredictX Challenge');
  }));
});
