import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IDashboard } from '../../models/dashboard.interface';

@Component({
  selector: 'app-hollywood',
  templateUrl: './hollywood.component.html',
  styleUrls: ['./hollywood.component.scss']
})

export class HollywoodComponent implements OnInit {
  @Input()
  hollywood: IDashboard[];
  @Output()
  dashboardSelected: EventEmitter<number> = new EventEmitter();

  constructor() {}

  navigateToDashboard(dashboard: IDashboard) {
    this.dashboardSelected.emit(dashboard.id);
  }

  ngOnInit() {

  }
}
