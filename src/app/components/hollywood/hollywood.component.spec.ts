import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HollywoodComponent } from './hollywood.component';
import { ChartModule } from 'angular-highcharts';
import {
  MatDividerModule,
  MatListModule,
  MatGridListModule,
  MatDialogModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatInputModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import { appReducers } from '../../store/reducers/app.reducers';
import { StoreModule } from '@ngrx/store';


describe('HollywoodComponent', () => {
  let component: HollywoodComponent;
  let fixture: ComponentFixture<HollywoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HollywoodComponent ],
      imports: [
        ChartModule,
        MatDividerModule,
        MatListModule,
        MatGridListModule,
        MatDialogModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatInputModule,
        MatSelectModule,
        MatToolbarModule,
        StoreModule.forRoot(appReducers)
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HollywoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
