import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IDashboard } from '../../models/dashboard.interface';

import * as actions from '../../store/actions/dashboard.actions';
import * as reducers from '../../store/reducers/dashboard.reducers';
import { IDashboardState } from '../../store/state/dashboard.state';
import { Chart } from 'angular-highcharts';

import { DashboardComponent } from './dashboard.component';
import { ChartModule } from 'angular-highcharts';
import {
  MatDividerModule,
  MatListModule,
  MatGridListModule,
  MatDialogModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatInputModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import { appReducers } from '../../store/reducers/app.reducers';
import { StoreModule } from '@ngrx/store';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      imports: [
        ChartModule,
        MatDividerModule,
        MatListModule,
        MatGridListModule,
        MatDialogModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatInputModule,
        MatSelectModule,
        MatToolbarModule,
        StoreModule.forRoot(appReducers)
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select the correct dashboard', () => {
    const dashboardList: IDashboard[] = [
      { id: 1, name: 'Dashboard 1', layout: 'grid1', style: 'dark', charts: [] },
      { id: 2, name: 'Dashboard 2', layout: 'grid1', style: 'dark', charts: [] }
    ];
    const initialState: IDashboardState = {
      hollywood: dashboardList,
      selectedDashboard: dashboardList[0]
    };

    const getSelectedDashboard = new actions.GetDashboardSuccess(dashboardList[1]);
    const state = reducers.dashboardReducers(initialState, getSelectedDashboard);

    expect(state.selectedDashboard).toEqual(dashboardList[1]);
  });

  it ('should create a chart', () => {
    component.dashboard = { id: 1, name: 'Dashboard 1', layout: 'grid1', style: 'dark', charts: [] };
    component.addChart();
    expect(component.charts.length).toBeGreaterThan(0);
    expect(component.charts[0] instanceof Chart).toBe(true);
    expect(component.dashboard.charts.length).toBeGreaterThan(0);
  });
});

