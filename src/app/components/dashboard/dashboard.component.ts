import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../store/state/app.state';
import { IDashboard } from '../../models/dashboard.interface';
import { selectSelectedDashboard } from '../../store/selectors/dashboard.selector';
import { EditDashboard } from '../../store/actions/dashboard.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  @Input() dashboard: IDashboard;
  charts: Chart[] = [];

  constructor(private _store: Store<IAppState>) {}

  ngOnInit() {
    this._store
      .pipe(select(selectSelectedDashboard))
      .subscribe(dashboard => {
        this.dashboard = dashboard;
        if (dashboard) {
          this.charts = [];
          for (const chartConfig of this.dashboard.charts) {
            const chart = new Chart(chartConfig);
            this.charts.push(chart);
            chart.ref$.subscribe();
          }
        }
      });
  }

  private generateData() {
    const arr = [];

    for (let i = 0; i < 20; i++) {
      arr.push(Math.round(100 * Math.random()));
    }

    return arr;
  }

  private getRandomArbitrary(min: number, max: number) {
    const random = Math.floor(Math.random() * (max - min)) + min;
    console.log('random', random);
    return random;
  }

  private selectSerieType() {
    const types = ['line', 'area', 'areaspline', 'spline', 'column'];
    return types[this.getRandomArbitrary(0, types.length)];
  }

  private generateSerieName(type: string) {
    return `${type.toUpperCase()}S SERIE`;
  }

  private generateSeries() {
    const series = [];
    const type = this.selectSerieType();
    series.push({
      name: this.generateSerieName(type),
      type: this.selectSerieType(),
      data: this.generateData()
    });
    return series;
  }

  private createChartConfig() {
    const newChartConfig = {
      chart: {
        type: 'line'
      },
      title: {
        text: `Widget ${this.dashboard.charts.length || 0 + 1}`
      },
      credits: {
        enabled: false
      },
      series: this.generateSeries()
    };

    return newChartConfig;
  }

  getClasses() {
    return `${this.dashboard.layout} ${this.dashboard.style}`;
  }

  addChart() {
    const newChartConfig = this.createChartConfig();
    const chart = new Chart(newChartConfig);

    if (this.dashboard && this.dashboard.charts && this.dashboard.charts instanceof Array) {
      this.dashboard.charts.push(newChartConfig);
    } else {
      this.dashboard.charts = new Array(newChartConfig);
    }
    this._store.dispatch(new EditDashboard(this.dashboard));
    this.charts.push(chart);
    chart.ref$.subscribe();
  }
}
